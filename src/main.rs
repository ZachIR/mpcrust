/* This file is part of mpcrust.
 *
 * Mpcrust is free software: you can redistribute it and/or modify it under the terms of the GNU
 * General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * Mpcrust is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with mpcrust. If not,
 * see <https://www.gnu.org/licenses/>.
 */

extern crate mpd;
extern crate getopts;

use mpd::error::Result;
use mpd::{Client,Song};
use mpd::State;
use mpd::Status;
use getopts::Options;
use std::env;
use std::net::TcpStream;

const DEFAULT_HOST: &str = "localhost";
const DEFAULT_PORT: u16 = 6600;

fn connect(host: String, port: u16) -> Result<Client<TcpStream>> {
	let conn = Client::connect(format!("{}:{}", host, port));
	return conn;
}

fn main() {
	let args: Vec<String> = env::args().collect();

	let mut opts = Options::new();
	opts.optopt("", "host", "The MPD server to connect to.\n\nDefault: the value of the environment variable MPD_HOST.", "HOST");
	opts.optopt("", "port", "The TCP port of the MPD server to connect to.\n\nDefault: the value of the environment variable MPD_PORT.", "PORT");
	opts.optflag("h", "help", "Print this help text.");
	let matches = match opts.parse(&args[1..]) {
		Ok(p) => p,
		Err(m) => {
			eprint!("{}", opts.usage(m.to_string().as_str()));
			return;
		}
	};
	if matches.opt_present("h") {
		print!("{}", opts.usage("mpcrust: an MPD client"));
		return;
	}

	let host: String = matches.opt_str("host")
		.or(env::var("MPD_HOST").ok())
		.unwrap_or(String::from(DEFAULT_HOST));

	let port: u16 = match matches.opt_str("port")
		.or(env::var("MPD_PORT").ok())
		.unwrap_or(DEFAULT_PORT.to_string())
		.parse() {
		Ok(p) => p,
		Err(m) => {
			eprintln!("Error while parsing port number ({})", m);
			return;
		}
	};

	println!("Connecting to {} port {}", &host, port);


	let mut conn = match connect(host, port) {
		Ok(c) => c,
		Err(e) => {
			eprintln!("Error while connecting to MPD server ({})", e);
			return;
		}
	};

	let stat: Status = conn.status().unwrap();

	let icon = match stat.state {
		State::Play => "▶",
		State::Pause => "⏸",
		State::Stop => "⏹",
	};

	let queue: Vec<Song> = conn.queue().unwrap();
	let current_queue_place = stat.song;

	let volume = stat.volume;

	if volume != -1 {
		println!("Volume: {}%", volume);
	}

	for (i, song) in queue.iter().enumerate() {
		let current = current_queue_place.map_or(false, |qp| qp.pos == i as u32);
		println!(
			"{} {}",
			if current {
				format!("  {} ", icon)
			} else {
				format!("{:3}.", i + 1)
			},
			song.title.as_ref().unwrap()
		);
	}

	if current_queue_place.is_none() {
		println!("{}", icon);
	}
}

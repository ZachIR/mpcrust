# mpcrust

Mpcrust (pronounce as *M P crust*) is an [MPD](https://www.musicpd.org/) client for your terminal.

**Development status: pre-alpha.**

Mpcrust is inspired by [ncmpcpp](https://rybczak.net/ncmpcpp/) but aims to have a cleaner layout,
better UX, and easier to maintain code base.

## Installation
There’s nothing to install yet!

## Development
Chat channel: join [#mpcrust](ircs://chat.freenode.net:6697/#mpcrust) on
[Freenode](https://freenode.net/). If you’re new to IRC,
[start here](https://meta.wikimedia.org/wiki/IRC/Instructions) (but join `#mpcrust` instead of
`#wikipedia`).
